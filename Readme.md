# Desafio recriar as funçoes filter e map

Exemplo:

```js
// Filter

const lista = [1, 2, 3, 4, 5]
console.log(lista.filter(item => item % 2 != 0))
// [1, 3, 5]
```

```js
// Map

const lista = [100, 200, 300, 400, 500]
console.log(lista.map(item => item / 100))
// [1, 2, 3, 4, 5]
```

### Como testar

python >= 3.8 

* Crie um ambiente virtual e instale as dependências
```bash
python3.8 -m venv .venv
pip install -r requirements.txt
```

* Rode os testes
```bash
pytest -vv -s -x
```
