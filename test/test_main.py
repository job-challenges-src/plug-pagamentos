from src.main import list_filter, list_map

def test_list_filter():
    """Shoulde be filter array by conditions"""
    before_filter_list = [1, 2, 3, 4, 5]
    after_filter_list = list_filter(before_filter_list, lambda item: item % 2 != 0)
    expected_filter_list = [1, 3, 5]
    assert after_filter_list == expected_filter_list


def test_list_map():
    """Shoulde be map array by conditions"""
    before_filter_list = [100, 200, 300, 400, 500]
    after_filter_list = list_map(before_filter_list, lambda item: item / 100)
    expected_filter_list = [1, 2, 3, 4, 5]
    assert after_filter_list == expected_filter_list