from typing import Any, Callable, List

def list_filter(array: List[Any], callback: Callable):
    return [i for i in array if callback(i)]

def list_map(array: List[Any], callback: Callable):
    return [callback(i) for i in array]
